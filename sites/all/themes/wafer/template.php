<?php

/**
 * @file
 * template.php
 */
function wafer_theme(&$existing, $type, $theme, $path) {
	bootstrap_include($theme, 'theme/system/page.vars.php');
	$hooks = array();
	return $hooks;
} 
function wafer_preprocess_region(&$variables, $hook) {
	if($variables['region'] == "footer"){
		$variables['classes_array'][] = 'col-xs-9';
	}
}

function wafer_form_alter(&$form, &$form_state, $form_id) {
  if($form_id=="contact_site_form"){  // print form ID to messages
  //dsm($form);  // pretty print array using Krumo to messages
  	$form['#attributes']['class'][] = 'col-md-6';
  }
}

