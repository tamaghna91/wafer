<?php
/**
 * @file
 * wafer_feature.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function wafer_feature_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-about_stanley'] = array(
    'cache' => -1,
    'css_class' => 'col-md-4',
    'custom' => 0,
    'machine_name' => 'about_stanley',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'wafer',
        'weight' => -9,
      ),
    ),
    'title' => 'About STANLEY',
    'visibility' => 0,
  );

  $export['block-contact_me'] = array(
    'cache' => -1,
    'css_class' => 'contact-me col-md-6',
    'custom' => 0,
    'machine_name' => 'contact_me',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'contact',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'wafer',
        'weight' => -11,
      ),
    ),
    'title' => 'Contact Me',
    'visibility' => 1,
  );

  $export['block-my_bunker'] = array(
    'cache' => -1,
    'css_class' => 'col-md-4',
    'custom' => 0,
    'machine_name' => 'my_bunker',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'wafer',
        'weight' => -11,
      ),
    ),
    'title' => 'MY BUNKER',
    'visibility' => 0,
  );

  $export['block-my_links'] = array(
    'cache' => -1,
    'css_class' => 'col-md-4',
    'custom' => 0,
    'machine_name' => 'my_links',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'wafer',
        'weight' => -10,
      ),
    ),
    'title' => 'My Links',
    'visibility' => 0,
  );

  $export['block-the_skills'] = array(
    'cache' => -1,
    'css_class' => 'the-skills col-lg-6',
    'custom' => 0,
    'machine_name' => 'the_skills',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'about',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'wafer',
        'weight' => -7,
      ),
    ),
    'title' => 'The Skills',
    'visibility' => 1,
  );

  $export['block-the_thinking'] = array(
    'cache' => -1,
    'css_class' => 'the-thinking col-lg-6',
    'custom' => 0,
    'machine_name' => 'the_thinking',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'about',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'wafer',
        'weight' => -8,
      ),
    ),
    'title' => 'The Thinking',
    'visibility' => 1,
  );

  $export['block-work_header'] = array(
    'cache' => -1,
    'css_class' => 'my-work col-lg-6',
    'custom' => 0,
    'machine_name' => 'work_header',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'work',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'wafer',
        'weight' => -10,
      ),
    ),
    'title' => 'My Work',
    'visibility' => 1,
  );

  $export['blog-recent'] = array(
    'cache' => 1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'recent',
    'module' => 'blog',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'dashboard_inactive',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wafer',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-features'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'features',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wafer',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['powr_resume-powr_resume'] = array(
    'cache' => 8,
    'css_class' => 'col-md-12',
    'custom' => 0,
    'delta' => 'powr_resume',
    'module' => 'powr_resume',
    'node_types' => array(),
    'pages' => 'resume',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'wafer',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['profile-author-information'] = array(
    'cache' => 5,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'author-information',
    'module' => 'profile',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wafer',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['search-form'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -1,
      ),
      'seven' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'seven',
        'weight' => -10,
      ),
      'wafer' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wafer',
        'weight' => -1,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-help'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'help',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'help',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'help',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wafer',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'css_class' => 'col-xs-12',
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'wafer',
        'weight' => -9,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wafer',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-navigation'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'navigation',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wafer',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-powered-by'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'powered-by',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 10,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 10,
      ),
      'wafer' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wafer',
        'weight' => -8,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-user-menu'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'user-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wafer',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 10,
      ),
      'wafer' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'wafer',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-about_me-block'] = array(
    'cache' => -1,
    'css_class' => 'about-me col-xs-12',
    'custom' => 0,
    'delta' => 'about_me-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>
about',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'wafer',
        'weight' => -13,
      ),
    ),
    'title' => 'About me',
    'visibility' => 1,
  );

  $export['views-my_portfolio-block'] = array(
    'cache' => -1,
    'css_class' => 'my-portfolio col-xs-12',
    'custom' => 0,
    'delta' => 'my_portfolio-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'wafer' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'wafer',
        'weight' => -12,
      ),
    ),
    'title' => 'My Portfolio',
    'visibility' => 1,
  );

  return $export;
}
