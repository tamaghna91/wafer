<?php
/**
 * @file
 * wafer_feature.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function wafer_feature_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'About STANLEY';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'about_stanley';
  $fe_block_boxes->body = 'This cute theme was created to showcase your work in a simple way. Use it wisely.';

  $export['about_stanley'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Contact us description';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'contact_me';
  $fe_block_boxes->body = '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>';

  $export['contact_me'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'My Address';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'my_bunker';
  $fe_block_boxes->body = 'Some Address 987,
+34 9054 5455, 
Madrid, Spain.';

  $export['my_bunker'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'My Links';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'my_links';
  $fe_block_boxes->body = '<a class="#">Dribbble</a>
<a class="#">Twitter</a>
<a class="#">Facebook</a>';

  $export['my_links'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'The Skills';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'the_skills';
  $fe_block_boxes->body = '<p>Drupal</p>
<div class="progress">
					<div class="progress-bar progress-bar-theme" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
						<span class="sr-only">40% Complete</span>
					</div>
				</div>
<p>Photshop</p>
<div class="progress">
					<div class="progress-bar progress-bar-theme" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
						<span class="sr-only">50% Complete</span>
					</div>
				</div>
<p>Bootstrap</p>
<div class="progress">
					<div class="progress-bar progress-bar-theme" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
						<span class="sr-only">60% Complete</span>
					</div>
				</div>
<p>HTML5, CSS3 & Javascript</p>
<div class="progress">
					<div class="progress-bar progress-bar-theme" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
						<span class="sr-only">80% Complete</span>
					</div>
				</div>';

  $export['the_skills'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'The Thinking';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'the_thinking';
  $fe_block_boxes->body = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.';

  $export['the_thinking'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Work Page Header';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'work_header';
  $fe_block_boxes->body = 'Show your work here. 
Dribbble shots from the awesome designer <a href="#>David Creighton-Pester</a>';

  $export['work_header'] = $fe_block_boxes;

  return $export;
}
