<?php
/**
 * @file
 * wafer_feature.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function wafer_feature_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'liking',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '5b7774d8-2184-4fe1-8310-4de0f484e48c',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'myself',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'bbf0bcd2-e2ca-40d6-9abe-25b34d3ae4e9',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'resume',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'eebe61fe-5676-4b98-bde1-479792ba0044',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'portfolio',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'fd843873-3f26-4471-8673-2f4f5a4e350a',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  return $terms;
}
